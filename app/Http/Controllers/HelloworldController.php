<?php

namespace App\Http\Controllers;

use App\Helloworld;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class HelloworldController extends Controller
{
    //
    public function index()
    {
        return Helloworld::all();
    } 
}
