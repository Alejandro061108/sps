<?php

use Illuminate\Database\Seeder;
use App\Helloworld;

class HelloworldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Helloworld::create([
            'id' => 1,
            'mensaje' => 'Ejercicio 2',
        ]);
    }
}
